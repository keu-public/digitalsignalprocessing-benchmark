#!/bin/bash

# Recover path to script
script_path="$(dirname "${0}")"

#mpi_command=$1/bin/mpiexec
mpi_command=mpirun

# pdot
$1/bin/pvbatch $script_path/benchDSP.py --filepath=$2 --paraview_dir $1 --baseline_dir $script_path/baselines/ --output_dir=$script_path/output/

# multiplex
for i in 1 2 4 8 16
do
  $mpi_command -np $i $1/bin/pvbatch $script_path/benchDSP.py --filepath=$2 --paraview_dir $1 --baseline_dir $script_path/baselines/ --output_dir=$script_path/output/ --multiplex
done
