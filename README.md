# DigitalSignalProcessing-Benchmark

A tool to benchmark DigitalSignalProcessing plugin

## About the dummy PSM

On first usage of a filter connected to both a temporal input and a continue_executing input,
the whole continue executing loop is re-executed, which is very expensive and should not be evaluated.
A dummy PSM is used to hide these timings.

## About data benchmarking

When evaluating execution time for data, the formating steps contains also all the IO.
There is no simple way to instrumentalize the IO and remove the time spent there.

## About modification of the python script

The `progression` can be modified depending of the memory capabilities and the evaluated input datasets.
Here we suggest one for evaluating a high number of timesteps with data:

`progression = [1e4, 5e3, 1e3, 5e2, 1e2, 5e1, 1e1]`

Another suggestion is to use a lower number of timesteps but a higher number of points, which is what was used
for analytical results:

`progression = [2e3, 1e3, 5e2, 1e2, 5e1, 1e1]`

## About modification of the bash script

The loop on number of processors can be modified depending on the capabilities on the host machine.

## How to use for analytic benchmark

```
VTK_SMP_BACKEND_IN_USE=Sequential /path/to/benchDSP.sh /path/to/paraview/install
```

## How to use for data benchmark

```
VTK_SMP_BACKEND_IN_USE=Sequential /path/to/benchDSP.sh /path/to/paraview/install /path/to/data.ext
```

## License

This repository is under the Apache 2.0 license. See LICENSE and NOTICE file.
