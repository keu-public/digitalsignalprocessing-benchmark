import os
import argparse
import time
import math
import gc
import csv
import numpy as np
from mpi4py import MPI
from paraview.simple import *

def compare_baseline(source, array, baselineDir, outputDir, baseline):

  repr = Show(source)
  ColorBy(repr, ('POINTS', array))
  view = Render()
  ResetCamera()

  import os
  baseline_file = os.path.join(baselineDir, baseline)
  from paraview.vtk.test import Testing
  Testing.VTK_TEMP_DIR = outputDir

  # This print all the time, no way around it
  Testing.compareImage(view.GetRenderWindow(), baseline_file, threshold=0)

  Hide(source)


def create_analytic_source(sphereResolution, nTimeSteps):

  # create a new 'Sphere'
  sphere1 = Sphere(registrationName='Sphere1')
  sphere1.Radius = 10.0
  sphere1.ThetaResolution = sphereResolution
  sphere1.PhiResolution = sphereResolution

  # create a new 'Generate Time Steps'
  generateTimeSteps1 = GenerateTimeSteps(registrationName='GenerateTimeSteps1', Input=sphere1)
  generateTimeSteps1.TimeStepValues = np.arange(0, 1, 1/nTimeSteps)

  # create a new 'Spatio Temporal Harmonics Attribute'
  spatioTemporalHarmonicsAttribute1 = GenerateSpatioTemporalHarmonics(registrationName='SpatioTemporalHarmonicsAttribute1', Input=generateTimeSteps1)
  spatioTemporalHarmonicsAttribute1.UpdatePropertyInformation();

  return spatioTemporalHarmonicsAttribute1

def create_data_source(inputFilePath, nTimeSteps):

  # Open provided data file
  reader = OpenDataFile(inputFilePath);
  reader.UpdatePropertyInformation()
  reader.UpdatePipeline()

  # create a new 'Generate Time Steps'
  generateTimeSteps1 = GenerateTimeSteps(registrationName='GenerateTimeSteps1', Input=reader)

  # filter time steps based on n time steps target
  ts = reader.TimestepValues
  tsDiff  = len(ts) - nTimeSteps
  if tsDiff > 0:
    ts = ts[: -tsDiff]
  generateTimeSteps1.TimeStepValues = ts

  return generateTimeSteps1
  
def add_formating_step(source, presArray, multiplex):

  if (multiplex):
    formatter = TemporalMultiplexing(registrationName='TemporalMultiplexing1', Input=source)
    formatter.DataArrays = [presArray]
  else:
    # create a new 'Plot Data Over Time'
    formatter = PlotDataOverTime(registrationName='PlotDataOverTime1', Input=source)
    formatter.OnlyReportSelectionStatistics = 0

  formatter.UpdatePipeline()
  return formatter

def add_fft_step(source, multiplex):

  if (multiplex):
    fft = DSPTableFFT(registrationName='fft', Input=source)
  else:
    fft = TableFFT(registrationName='fft', Input=source)

  fft.OneSidedSpectrum = True
  fft.UpdatePipeline()
  return fft

def bench_dsp(sphereResolution, nTimeSteps, inputFilePath, multiplex, baselineDir, outputDir):
  ret_list = []

  # ----------------------------------------------------------------
  # setup views used in the visualization
  # ----------------------------------------------------------------

  # Create a new 'Render View'
  renderView2 = CreateView('RenderView')
  renderView2.ViewSize = [300, 300]
  SetActiveView(renderView2)

  # ----------------------------------------------------------------
  # setup the data processing pipelines
  # ----------------------------------------------------------------

  if (not inputFilePath):
    source = create_analytic_source(sphereResolution, nTimeSteps)
  else:
    source = create_data_source(inputFilePath, nTimeSteps)
  source.UpdatePropertyInformation();
  source.UpdatePipeline()

  # Recover the scalars name, if not available, the first array
  dataInfo = source.GetDataInformation().DataInformation.GetPointDataInformation()
  presArray = ""
  arrayInfo = dataInfo.GetAttributeInformation(paraview.vtk.vtkDataObject.POINT)
  if (arrayInfo):
    presArray = dataInfo.GetAttributeInformation(paraview.vtk.vtkDataObject.POINT).GetName()
  if (not presArray):
    presArray = dataInfo.GetArrayInformation(0).GetName()

  # ----------------------------------------------------------------
  # Initialize benchmarch
  # ----------------------------------------------------------------

  # nPoints
  ret_list.append(source.GetDataInformation().DataInformation.GetNumberOfPoints())
  # nTimeSteps
  ret_list.append(source.GetDataInformation().DataInformation.GetNumberOfTimeSteps())

  tic = time.perf_counter()

  # ----------------------------------------------------------------
  # Formating data benchmark
  # ----------------------------------------------------------------

  formatter = add_formating_step(source, presArray, multiplex)

  toc = time.perf_counter()
  ret_list.append(toc - tic)

  # ----------------------------------------------------------------
  # Sound Quantities Calculator benchmark
  # ----------------------------------------------------------------
  tic = time.perf_counter()

  # create a new 'Sound Quantities Calculator'
  soundQuantitiesCalculator1 = SoundQuantitiesCalculator(registrationName='SoundQuantitiesCalculator1', TableData=formatter,
      DestinationMesh=source)
  soundQuantitiesCalculator1.PressureColumn = presArray
  soundQuantitiesCalculator1.UpdatePipeline()

  toc = time.perf_counter()
  ret_list.append(toc - tic)

  if (not inputFilePath):
    compare_baseline(soundQuantitiesCalculator1, 'RMS Pressure (dB)', baselineDir, outputDir ,f"SQCExpectedResult_{sphereResolution}_{nTimeSteps}_img.png")

  # ----------------------------------------------------------------
  # DSP table FFT
  # ----------------------------------------------------------------
  tic = time.perf_counter()

  fft = add_fft_step(formatter, multiplex)

  toc = time.perf_counter()
  ret_list.append(toc - tic)

  # ----------------------------------------------------------------
  # Project spectrum magnitude benchmark
  # ----------------------------------------------------------------
  tic = time.perf_counter()

  # create a new 'Project Spectrum Magnitude'
  projectSpectrumMagnitude1 = ProjectSpectrumMagnitude(registrationName='ProjectSpectrumMagnitude1', TableData=fft,
      DestinationMesh=source)
  projectSpectrumMagnitude1.ColumnsToProject = [presArray]
  projectSpectrumMagnitude1.FrequencyArray = ['ROWS', 'Frequency']
  projectSpectrumMagnitude1.LowerFrequency = 0
  projectSpectrumMagnitude1.UpperFrequency = 50
  projectSpectrumMagnitude1.UpdatePipeline()

  toc = time.perf_counter()
  ret_list.append(toc - tic)

  if (not inputFilePath):
    compare_baseline(projectSpectrumMagnitude1, presArray, baselineDir, outputDir,f"PSMExpectedResult_{sphereResolution}_{nTimeSteps}_img.png")

  # ----------------------------------------------------------------
  # Cleanup
  # ----------------------------------------------------------------

  ResetSession()
  return ret_list

parser = argparse.ArgumentParser()
parser.add_argument('-x', '--multiplex', action=argparse.BooleanOptionalAction)
parser.add_argument('-f', '--filepath')
parser.add_argument('-p', '--paraview_dir')
parser.add_argument('-b', '--baseline_dir')
parser.add_argument('-o', '--output_dir')
args = parser.parse_args()
multiplex = args.multiplex
filepath = args.filepath
paraview_dir = args.paraview_dir
baseline_dir = args.baseline_dir
output_dir = args.output_dir

if (multiplex):
  format_string = "multiplex"
else:
  format_string = "pdot"

if (not filepath):
  basename = "analytic"
else :
  basename = os.path.basename(filepath)

LoadPlugin(f'{paraview_dir}/lib/paraview-5.11/plugins/DigitalSignalProcessing/DigitalSignalProcessing.so', remote=False, ns=globals())
paraview_version = GetParaViewSourceVersion().split(' ')[2];

comm = MPI.COMM_WORLD
nprocs = comm.Get_size()

logfile = f'dsp_bench_{basename}_{format_string}_{nprocs}_{paraview_version}.csv'
print(logfile)
with open(f'{output_dir}/{logfile}', 'w', newline='') as csvfile:
  writer = csv.writer(csvfile, delimiter=',')
  writer.writerow(['nPoints'] + ['nTimeSteps'] + ['FormatTime (s)'] + ['SQCTime (s)'] + ['FFTTime (s)'] + ['PSMTime (s)'] + ['nProcs'])

  # Adapt this progression depending on host machine memory capabilities
  # progression = [1e4, 5e3, 1e3, 5e2, 1e2, 5e1, 1e1]
  progression = [2e3, 1e3, 5e2, 1e2, 5e1, 1e1]
  # progression = [1e2]

  if not (filepath):
    progressionData = progression
  else:
    # No data progression when using files
    progressionData = [1];
    print(f"input file: {filepath}")

  for reso in progressionData:
    for nTS in progression:
      print(f"reso: {reso} nTS: {nTS}")
      data = []
      for i in range(3):
        single_bench = bench_dsp(int(reso), int(nTS), filepath, multiplex, baseline_dir, output_dir)
        data.append(single_bench)
      data = np.average(np.array(data), axis=0)
      data = np.append(data, [nprocs])
      writer.writerow(data)
      gc.collect()
